package com.ehsanfahad.whereisthebus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ehsanfahad.whereisthebus.databinding.LayoutSimpleRecyclerViewBinding;

import java.util.ArrayList;


public class SimpleRecyclerViewAdapter
        extends RecyclerView.Adapter < SimpleRecyclerViewAdapter.ViewHolder >
{

    // Fields
    private Context context;
    private ArrayList < SimpleRecyclerViewItem > items;


    // Constructor
    public SimpleRecyclerViewAdapter(@NonNull Context context,
                                     @NonNull ArrayList<SimpleRecyclerViewItem> items)
    {
        this.context = context;
        this.items = items;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutSimpleRecyclerViewBinding binding
                = LayoutSimpleRecyclerViewBinding.inflate(LayoutInflater.from(context));
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SimpleRecyclerViewItem currentItem = items.get(position);

        holder.binding.setAdapterPosition(position);
        holder.binding.setTitle(currentItem.title);
        holder.binding.setSubtitle(currentItem.subtitle);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder
    {
        LayoutSimpleRecyclerViewBinding binding;

        public ViewHolder(@NonNull LayoutSimpleRecyclerViewBinding binding)
        {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
