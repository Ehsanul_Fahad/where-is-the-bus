package com.ehsanfahad.whereisthebus;


public class SimpleRecyclerViewItem
{

    // Fields
    public String title;
    public String subtitle;


    /**
     * Constructor
     * @param title     Title text
     * @param subtitle  Subtitle text
     */
    public SimpleRecyclerViewItem(String title, String subtitle)
    {
        this.title = title;
        this.subtitle = subtitle;
    }

}
