package com.ehsanfahad.whereisthebus;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.ehsanfahad.whereisthebus.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{

    private ActivityMainBinding binding;
    private WifiManager wifiManager;
    private Handler handler = new Handler();
    private Runnable runnable = null;


    private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            if ((intent != null)
                    && (intent.getAction() != null)
                    && intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                List<ScanResult> mScanResults = wifiManager.getScanResults();
                updateRecyclerView(mScanResults);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setLifecycleOwner(this);

        setUpComponents();
        checkForPermissions();
    }

    @Override
    protected void onStop() {
        if (handler != null && runnable != null)
            handler.removeCallbacks(runnable);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (handler != null && runnable != null)
            handler.removeCallbacks(runnable);
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.CHECK_PERMISSION_REQUEST_CODE && grantResults.length > 0) {
            boolean allGranted = true;

            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    allGranted = false;
                    break;
                }
            }

            if (allGranted)
                getAllWifiSourceList();
            else
                Toast.makeText(this, "Permission denied", Toast.LENGTH_LONG).show();
        }
    }


    private void setUpComponents()
    {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        registerReceiver(mWifiScanReceiver,
                new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    private void getAllWifiSourceList()
    {
        Log.d("MY_LOGGER", "scanning started.");
        wifiManager.startScan();

        runnable = new Runnable() {
            @Override
            public void run() {
                wifiManager.startScan();

                handler.postDelayed(runnable, 5000);
            }
        };

        handler.postDelayed(runnable, 5000);
    }

    private void checkForPermissions()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean permissionsGranted;
            permissionsGranted = (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
            permissionsGranted &= (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
            permissionsGranted &= (checkSelfPermission(Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED);
            permissionsGranted &= (checkSelfPermission(Manifest.permission.CHANGE_WIFI_STATE) == PackageManager.PERMISSION_GRANTED);

            if (!permissionsGranted) {
                requestPermissions(
                        new String[] {
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_WIFI_STATE,
                                Manifest.permission.CHANGE_WIFI_STATE,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        Constants.CHECK_PERMISSION_REQUEST_CODE
                );

            } else
                getAllWifiSourceList();

        } else
            getAllWifiSourceList();
    }

    private void updateRecyclerView(List<ScanResult> scanResults)
    {
        ArrayList < SimpleRecyclerViewItem > items = new ArrayList<>();

        for (ScanResult scanResult : scanResults) {
            items.add(new SimpleRecyclerViewItem(
                    scanResult.SSID,
                    scanResult.BSSID
            ));
        }

        binding.recyclerView.setAdapter(new SimpleRecyclerViewAdapter(this, items));
    }

}
